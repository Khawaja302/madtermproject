package com.example.hamzaabid.blooddonars.NetworkCalls;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static com.example.hamzaabid.blooddonars.Activities.UserPanel.IP_ADDRESS;

/**
 * Created by Hamza Abid on 5/13/2017.
 */


public class UpdateDonarAsyncTask extends AsyncTask<Void , Void , String> {
    String name , dob , bloodgroup , city , mobile , lastdonated;
    Activity context;
    int id , responsecode;
    public UpdateDonarAsyncTask(String name, String dob, String bloodgroup, String city, String mobile, String lastdonated, int id , Activity context) {
        this.name = name;
        this.dob = dob;
        this.bloodgroup = bloodgroup;
        this.city = city;
        this.mobile = mobile;
        this.lastdonated = lastdonated;
        this.id = id;
        this.context = context;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(Void... params) {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("name" , name)
                .add("dob", dob)
                .add("bloodgroup" , bloodgroup)
                .add("city" , city)
                .add("mobile" , mobile)
                .add("last_donated" , lastdonated)
                .build();
        Request request = new Request.Builder()
                .url("http://"+IP_ADDRESS+"/UolBloodDonationApp/api/uol"+"/"+id)
                .put(formBody)
                .build();
        String jsonstring = null;
        try {
            Response response = client.newCall(request).execute();
            jsonstring = (response.body().string());
            responsecode = response.code();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonstring;
    }

    @Override
    protected void onPostExecute(String jsondata) {
        if (responsecode==200) {
            Toast.makeText(context, "Data Updated ! " + jsondata, Toast.LENGTH_SHORT).show();
            DonarslistAsynctask donarslistAsynctask = new DonarslistAsynctask(context);
            donarslistAsynctask.execute();
        }
        else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }
}
