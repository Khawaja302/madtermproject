package com.example.hamzaabid.blooddonars.NetworkCalls;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static com.example.hamzaabid.blooddonars.Activities.UserPanel.IP_ADDRESS;

/**
 * Created by Hamza Abid on 5/12/2017.
 */

public class AddDonarAsyncTask extends AsyncTask<Void, Void, String> {
    Activity context;
    String name;
    String dob;
    String bloodgroup;
    String city;
    String mobile;
    String lastdonated;
    int responsecode;
    public AddDonarAsyncTask(Activity context, String name, String dob, String bloodgroup, String city, String mobile, String lastdonated) {
        this.context = context;
        this.name = name;
        this.dob = dob;
        this.bloodgroup = bloodgroup;
        this.city = city;
        this.mobile = mobile;
        this.lastdonated = lastdonated;
    }

    @Override
    protected String doInBackground(Void... params)
    {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("name", name)
                .add("dob", dob)
                .add("bloodgroup" , bloodgroup)
                .add("city" , city)
                .add("mobile" , mobile)
                .add("last_donated" , lastdonated)
                .build();
        Request request = new Request.Builder()
                .url("http://"+IP_ADDRESS+"/UolBloodDonationApp/api/uol")
                .post(formBody)
                .build();
        String jsonstring = null;
        try {
            Response response = client.newCall(request).execute();
            jsonstring = (response.body().string());
            responsecode = response.code();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonstring;
    }

    @Override
    protected void onPostExecute(String s) {
            if (responsecode==200) {
                if (s.equals("true")) {
                    Toast.makeText(context, "Donar Added " + s, Toast.LENGTH_SHORT).show();
                    DonarslistAsynctask donarslistAsynctask = new DonarslistAsynctask(context);
                    donarslistAsynctask.execute();
                } else {
                    Toast.makeText(context, "All Fields Are Required !", Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
    }
}
