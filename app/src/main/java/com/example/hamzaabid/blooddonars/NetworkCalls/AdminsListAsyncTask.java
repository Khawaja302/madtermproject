package com.example.hamzaabid.blooddonars.NetworkCalls;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.example.hamzaabid.blooddonars.Activities.AdminPanel;
import com.example.hamzaabid.blooddonars.Fragments.AdminslistFragment;
import com.example.hamzaabid.blooddonars.Fragments.ListViewFragment;
import com.example.hamzaabid.blooddonars.POJO.Admin;
import com.example.hamzaabid.blooddonars.POJO.Donar;
import com.example.hamzaabid.blooddonars.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import static com.example.hamzaabid.blooddonars.Activities.UserPanel.IP_ADDRESS;

/**
 * Created by Hamza Abid on 5/21/2017.
 */

public class AdminsListAsyncTask extends AsyncTask<Object, Object, String> {

    ProgressDialog loading;
    Activity context;
    int responsecode;

    public AdminsListAsyncTask(Activity context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(context, "Fetching Data...", "Please wait...", false, false);
    }

    @Override
    protected String doInBackground(Object... params) {
        OkHttpClient client = new OkHttpClient();
        Response response;
        Request request = new Request.Builder().url("http://"+IP_ADDRESS+"/UolBloodDonationApp/api/admin").build();
        String jsonarray = null;
        try {
            response = client.newCall(request).execute();
            jsonarray = response.body().string();
            responsecode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonarray;
    }
    @Override
    protected void onPostExecute(String jsondata) {
        if (responsecode==200) {

            Gson gson = new Gson();
            List<Admin> admins = gson.fromJson(jsondata, new TypeToken<ArrayList<Admin>>() {
            }.getType());
            loading.dismiss();

            FragmentManager fragmentManager = ((AdminPanel) context).getSupportFragmentManager();
            AdminslistFragment adminslistFragment = new AdminslistFragment(context, admins);
            fragmentManager.beginTransaction().replace(R.id.assetView_relative_layout, adminslistFragment).commit();
        }
        else {
            loading.dismiss();
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }

    }

}
