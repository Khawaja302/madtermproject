package com.example.hamzaabid.blooddonars.NetworkCalls;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.hamzaabid.blooddonars.Activities.UserPanel.IP_ADDRESS;

/**
 * Created by Hamza Abid on 5/13/2017.
 */

public class DeleteDonarAsyncTask extends AsyncTask<Void, Void, String> {
    Activity context;
    int id , responsecode;

    public DeleteDonarAsyncTask(Activity context, int id) {
        this.context = context;
        this.id = id;
    }

    @Override
    protected String doInBackground(Void... params) {
        OkHttpClient client = new OkHttpClient();
       /* RequestBody formBody = new FormBody.Builder()
                .build();*/
        //   HttpUrl url = new HttpUrl.Builder().host("http://192.168.10.4/UolBloodDonationApp/api/uol").
        Request request = new Request.Builder()
                .url("http://"+IP_ADDRESS+"/UolBloodDonationApp/api/uol" + "/" + id)
                .delete()
                .build();

        String jsonstring = null;
        try {
            Response response = client.newCall(request).execute();
            jsonstring = (response.body().string());
            responsecode = response.code();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonstring;
    }

    @Override
    protected void onPostExecute(String jsonresponse) {
        if (responsecode==200)
        {
        if (jsonresponse.equals("true"))
            Toast.makeText(context, "Donar Deleted !", Toast.LENGTH_SHORT).show();
            DonarslistAsynctask donarslistAsynctask = new DonarslistAsynctask(context);
            donarslistAsynctask.execute();
        }
        else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }
}
