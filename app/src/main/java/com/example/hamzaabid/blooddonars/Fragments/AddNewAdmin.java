package com.example.hamzaabid.blooddonars.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.hamzaabid.blooddonars.NetworkCalls.AddNewAdminAsyncTask;
import com.example.hamzaabid.blooddonars.NetworkCalls.EmailAlreadyExists;
import com.example.hamzaabid.blooddonars.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewAdmin extends Fragment {

   private EditText adminname , adminemail , adminpassword;
    Button addnewadmin;
    Activity activity;

    public AddNewAdmin(Activity activity) {
        this.activity = activity;
    }

    public AddNewAdmin() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_new_admin, container, false);
        adminname = (EditText) view.findViewById(R.id.adminname);
        adminemail = (EditText) view.findViewById(R.id.adminemail);
        adminpassword = (EditText) view.findViewById(R.id.adminpassword);
        addnewadmin = (Button) view.findViewById(R.id.addadmin);
        addnewadmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EmailAlreadyExists emailAlreadyExists = new EmailAlreadyExists(adminname.getText().toString()
                ,adminemail.getText().toString() , adminpassword.getText().toString() , activity);
                emailAlreadyExists.execute();
            }
        });

        return view;
    }

}
