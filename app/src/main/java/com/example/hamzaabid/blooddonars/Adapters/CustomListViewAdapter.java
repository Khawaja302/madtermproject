package com.example.hamzaabid.blooddonars.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hamzaabid.blooddonars.POJO.Donar;
import com.example.hamzaabid.blooddonars.R;

import java.util.List;

/**
 * Created by Hamza Abid on 5/7/2017.
 */

public class CustomListViewAdapter extends ArrayAdapter<Donar> {

    Context context;
    Activity activity;
    int imgres = R.drawable.mard;

    public CustomListViewAdapter(@NonNull Context context ,  @NonNull List<Donar> objects) {
        super(context,0, objects);
        activity= (Activity) context;
        this.context=context;
    }

    private static class ViewHolder {
        TextView txtName;
        TextView txtBloodGroup;
        TextView city;
        ImageView img;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Donar donar=getItem(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.customlistviewlayout, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.textView7);
            viewHolder.txtBloodGroup = (TextView) convertView.findViewById(R.id.textView8);
            viewHolder.city = (TextView) convertView.findViewById(R.id.textView9);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.imageview);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.txtName.setText(donar.getName());
        viewHolder.txtBloodGroup.setText(donar.getBloodgroup().toUpperCase());
        viewHolder.city.setText(donar.getCity().toUpperCase());
        viewHolder.img.setImageResource(imgres);
        return convertView;
    }
}
