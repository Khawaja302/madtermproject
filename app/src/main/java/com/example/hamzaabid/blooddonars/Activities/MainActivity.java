package com.example.hamzaabid.blooddonars.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hamzaabid.blooddonars.R;

import com.example.hamzaabid.blooddonars.Fragments.LoginFragment;
import com.example.hamzaabid.blooddonars.NetworkCalls.DonarslistAsynctask;

public class MainActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Shared Preferences checking files if the admin is logged in ...

        SharedPreferences sp = getSharedPreferences("login" , MODE_PRIVATE);
        String status = sp.getString("status" , "");
        if(status.equals("")) {

            DonarslistAsynctask donarslist = new DonarslistAsynctask(this);
            donarslist.execute();
            final TextView blooddonars = (TextView) findViewById(R.id.blooddonar);
            final Button login = (Button) findViewById(R.id.login);

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    login.setVisibility(View.INVISIBLE);
                    blooddonars.setText("Login");
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    LoginFragment loginFragment = new LoginFragment(getApplicationContext());
                    fragmentManager.beginTransaction().replace(R.id.listfragment_container, loginFragment).commit();
                }
            });
        }

        else {
            Intent intent = new Intent(getApplicationContext(), AdminPanel.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
            finish();
        }
    }
    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }
}
