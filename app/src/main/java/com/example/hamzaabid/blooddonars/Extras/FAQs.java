package com.example.hamzaabid.blooddonars.Extras;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hamzaabid.blooddonars.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FAQs extends Fragment {


    public FAQs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_faqs, container, false);

        return view;
    }

}
