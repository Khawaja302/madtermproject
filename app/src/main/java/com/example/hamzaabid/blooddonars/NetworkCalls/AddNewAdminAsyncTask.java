package com.example.hamzaabid.blooddonars.NetworkCalls;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import static com.example.hamzaabid.blooddonars.Activities.UserPanel.IP_ADDRESS;

/**
 * Created by Hamza Abid on 5/20/2017.
 */

public class AddNewAdminAsyncTask extends AsyncTask<Object, Object, String>
{
    int responsecode;
    String name , email , password;
    Activity activity;

    public AddNewAdminAsyncTask(String name, String email, String password, Activity activity) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.activity = activity;
    }

    @Override
    protected String doInBackground(Object... params) {
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("name", name)
                .add("email", email)
                .add("password" , password)
                .build();
        Request request = new Request.Builder()
                .url("http://"+IP_ADDRESS+"/UolBloodDonationApp/api/admin")
                .post(formBody)
                .build();
        String jsonstring = null;
        try {
            Response response = client.newCall(request).execute();
            jsonstring = (response.body().string());
            responsecode = response.code();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonstring;
    }
    @Override
    protected void onPostExecute(String jsondata) {
        if (responsecode==200) {
            if (jsondata.equals("true")) {
                Toast.makeText(activity, "Admin Created", Toast.LENGTH_SHORT).show();
                AdminsListAsyncTask admin = new AdminsListAsyncTask(activity);
                admin.execute();
            } else {
                Toast.makeText(activity, "Some Error Occured !", Toast.LENGTH_SHORT).show();

            }
        }
        else {
            Toast.makeText(activity, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

}
