package com.example.hamzaabid.blooddonars.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.hamzaabid.blooddonars.POJO.Donar;
import com.example.hamzaabid.blooddonars.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class Details extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        TextView name = (TextView) findViewById(R.id.name);

        Intent intent = this.getIntent();
        Gson gson = new Gson();
        String jsonstring = getIntent().getStringExtra("Object");
        int position = intent.getIntExtra("value : ", 0);
        List<Donar> donarslist = gson.fromJson(jsonstring , new TypeToken<ArrayList<Donar>>(){}.getType() );

        name.setText(donarslist.get(position).getName());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
