package com.example.hamzaabid.blooddonars.NetworkCalls;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.hamzaabid.blooddonars.Activities.AdminPanel;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.hamzaabid.blooddonars.Activities.UserPanel.IP_ADDRESS;

/**
 * Created by Hamza Abid on 5/9/2017.
 */

public class LoginAsynctask extends AsyncTask<Void , Void , String> {
    String email;
    String password;
    Context context;
    int responsecode;

    public LoginAsynctask(String email, String password, Context context){
        this.email = email;
        this.password = password;
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params) {

        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormBody.Builder()
                .add("email", email)
                .add("password", password)
                .build();
        Request request = new Request.Builder()
                .url("http://"+IP_ADDRESS+"/UolBloodDonationApp/api/loginAPI")
                .post(formBody)
                .build();

        String jsonstring = null;
        try {
            Response response = client.newCall(request).execute();
            jsonstring = (response.body().string());
            responsecode = response.code();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonstring;
    }

    @Override
    protected void onPostExecute(String s) {
        if (responsecode == 200) {
            if (s.equals("true")) {

                // SharedPreferences File Editing ...

                SharedPreferences sharedemail = context.getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor emaileditor = sharedemail.edit();
                emaileditor.putString("status", "True").commit();

                Intent intent = new Intent(context.getApplicationContext(), AdminPanel.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                context.startActivity(intent);
            } else
                Toast.makeText(context, "Email or password is not matching the database ...", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }


}
