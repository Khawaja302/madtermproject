package com.example.hamzaabid.blooddonars.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hamzaabid.blooddonars.Fragments.AddNewAdmin;
import com.example.hamzaabid.blooddonars.Fragments.AdminslistFragment;
import com.example.hamzaabid.blooddonars.Fragments.IssueNotification;
import com.example.hamzaabid.blooddonars.Fragments.MessagesList;
import com.example.hamzaabid.blooddonars.NetworkCalls.AdminsListAsyncTask;
import com.example.hamzaabid.blooddonars.NetworkCalls.FeedsList;
import com.example.hamzaabid.blooddonars.NetworkCalls.MessagesListAsyncTask;
import com.example.hamzaabid.blooddonars.R;

import com.example.hamzaabid.blooddonars.Fragments.AddDonarFragment;
import com.example.hamzaabid.blooddonars.NetworkCalls.DonarslistAsynctask;

public class AdminPanel extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Activity myActivity;
    TextView adminname, adminemail;
    public static FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_panel);
        myActivity = this;
        Intent intent = getIntent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        adminemail = (TextView) findViewById(R.id.adminemail);
        adminname = (TextView) findViewById(R.id.adminname);

        // Floating Action Button ...
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        DonarslistAsynctask donarslist = new DonarslistAsynctask(myActivity);
        donarslist.execute();
        fab.setVisibility(View.VISIBLE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDonarFragment addDonarFragment = new AddDonarFragment(myActivity);
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.assetView_relative_layout, addDonarFragment).commit();
                fab.setVisibility(View.INVISIBLE);
            }
        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (exit) {
                finish();
            } else {
                Toast.makeText(this, "Press Back again to Exit.",
                        Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_panel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return Boolean.parseBoolean(null);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragmentManager = getSupportFragmentManager();


        if (id == R.id.list) {
            DonarslistAsynctask donarslist = new DonarslistAsynctask(myActivity);
            donarslist.execute();
            fab.setVisibility(View.VISIBLE);
        } else if (id == R.id.donaradd) {
            fragmentManager.beginTransaction().replace(R.id.assetView_relative_layout, new AddDonarFragment(myActivity)).commit();
            fab.setVisibility(View.INVISIBLE);
        } else if (id == R.id.adminlist) {
            AdminsListAsyncTask adminsListAsyncTask = new AdminsListAsyncTask(myActivity);
            adminsListAsyncTask.execute();
            fab.setVisibility(View.INVISIBLE);
        } else if (id == R.id.msgs) {
            MessagesListAsyncTask messagesListAsyncTask = new MessagesListAsyncTask(myActivity);
            messagesListAsyncTask.execute();
            fab.setVisibility(View.INVISIBLE);
        } else if (id == R.id.feedslist) {
            FeedsList feedsList = new FeedsList(myActivity);
            feedsList.execute();
        } else if (id == R.id.logout) {
            // Shared Preferences Editing File ...
            SharedPreferences sharedemail = getSharedPreferences("login", MODE_PRIVATE);
            SharedPreferences.Editor emaileditor = sharedemail.edit();
            emaileditor.putString("status", "").commit();

            Intent mainactivity = new Intent(getApplicationContext(), UserPanel.class);
            mainactivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mainactivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainactivity);
            finish();

        } else if (id == R.id.noti) {
            fragmentManager.beginTransaction().replace(R.id.assetView_relative_layout, new IssueNotification(myActivity)).commit();
            fab.setVisibility(View.INVISIBLE);
        } else if (id == R.id.adminadd) {
            fragmentManager.beginTransaction().replace(R.id.assetView_relative_layout, new AddNewAdmin(myActivity)).commit();
            fab.setVisibility(View.INVISIBLE);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
