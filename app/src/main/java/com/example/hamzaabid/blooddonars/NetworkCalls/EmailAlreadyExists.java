package com.example.hamzaabid.blooddonars.NetworkCalls;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.hamzaabid.blooddonars.Activities.UserPanel.IP_ADDRESS;
import static com.example.hamzaabid.blooddonars.R.id.adminemail;
import static com.example.hamzaabid.blooddonars.R.id.adminname;
import static com.example.hamzaabid.blooddonars.R.id.adminpassword;

/**
 * Created by Hamza Abid on 5/22/2017.
 */

public class EmailAlreadyExists extends AsyncTask<Object, Object, String> {
    String name;
    String email;
    String password;
    Activity activity;
    int responsecode;

    public EmailAlreadyExists(String name, String email, String password, Activity activity) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.activity = activity;
    }


    @Override
    protected String doInBackground(Object... params) {
        OkHttpClient client = new OkHttpClient();
        RequestBody formbody = new FormBody.Builder().add("email", email).build();
        Request request = new Request.Builder().url("http://" + IP_ADDRESS + "/UolBloodDonationApp/api/checkemail")
                .post(formbody).build();
        String json = null;
        try {
            Response response = client.newCall(request).execute();
            json = (response.body().string());
            responsecode = response.code();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    protected void onPostExecute(String json) {
        if (responsecode == 200) {
            if (json.equals("true")) {
                AddNewAdminAsyncTask addNewAdminAsyncTask = new AddNewAdminAsyncTask(name, email, password, activity);
                addNewAdminAsyncTask.execute();
            } else {
                Toast.makeText(activity, "Email Already Exist", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(activity, "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }
}
