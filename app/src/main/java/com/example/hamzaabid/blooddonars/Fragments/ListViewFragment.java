package com.example.hamzaabid.blooddonars.Fragments;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.hamzaabid.blooddonars.Activities.AdminPanel;
import com.example.hamzaabid.blooddonars.Activities.UserPanel;
import com.example.hamzaabid.blooddonars.Adapters.CustomListViewAdapter;
import com.example.hamzaabid.blooddonars.NetworkCalls.DeleteDonarAsyncTask;
import com.example.hamzaabid.blooddonars.POJO.Donar;
import com.example.hamzaabid.blooddonars.Activities.MainActivity;
import com.example.hamzaabid.blooddonars.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListViewFragment extends Fragment {

    CustomListViewAdapter customListViewAdapter;
    Activity context;
    List<Donar> donarslist, temp ;
    List<Donar> searchResults;
    ListView listView;
    EditText search;
    View view;

    public ListViewFragment(Activity context, List<Donar> list) {
        this.context = context;
        this.donarslist = list;
        this.searchResults = list;
    }
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_list_view, container, false);
        listView = (ListView) view.findViewById(R.id.listview);
        search = (EditText) view.findViewById(R.id.search);
        temp = new ArrayList<Donar>(donarslist);
        customListViewAdapter = new CustomListViewAdapter(context, searchResults);
        listView.setAdapter(customListViewAdapter);

        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //get the text in the EditText
                //String searchString =search.getText().toString();
                int textLength = s.length();
                searchResults.clear();

                for (int i = 0; i < temp.size(); i++) {
                    String name = temp.get(i).getName().toString();
                    int lengthName = name.length();
                    String city = temp.get(i).getCity().toString();
                    int lengthCity = city.length();
                    String bloodGroup = temp.get(i).getBloodgroup().toString();
                    int lengthBloodGroup = bloodGroup.length();



                    //compare the String in EditText with Names in the ArrayList
                    if (textLength <= lengthBloodGroup && textLength <= lengthCity && textLength <= lengthName) {
                        if (s.toString().equalsIgnoreCase(bloodGroup.substring(0, textLength)) || s.toString().equalsIgnoreCase(city.substring(0, textLength)) || s.toString().equalsIgnoreCase(name.substring(0, textLength))) {
                            searchResults.add(temp.get(i));
                        }
                    } else if (textLength <= lengthCity && textLength <= lengthName) {
                        if (s.toString().equalsIgnoreCase(city.substring(0, textLength)) || s.toString().equalsIgnoreCase(name.substring(0, textLength))) {
                            searchResults.add(temp.get(i));
                        }
                    } else if (textLength <= lengthName) {
                        if (s.toString().equalsIgnoreCase(name.substring(0, textLength))) {
                            searchResults.add(temp.get(i));
                        }
                    } else if (textLength <= lengthCity) {
                        if (s.toString().equalsIgnoreCase(city.substring(0, textLength))) {
                            searchResults.add(temp.get(i));
                        }
                    }
                }
                customListViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {
                // some code ...
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
                if (context instanceof AdminPanel) {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(context);

                    alert.setMessage("What do you wanna do ?").setCancelable(false).setPositiveButton("Update", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UpdateDonarFragment updateDonarFragment = new UpdateDonarFragment(context, donarslist.get(position).getName()
                                    , donarslist.get(position).getDob()
                                    , donarslist.get(position).getBloodgroup()
                                    , donarslist.get(position).getCity()
                                    , donarslist.get(position).getMobile()
                                    , donarslist.get(position).getLast_donated()
                                    , donarslist.get(position).getId());

                            FragmentManager fragmentManager = ((AdminPanel) context).getSupportFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.assetView_relative_layout, updateDonarFragment).commit();
                            AdminPanel.fab.setVisibility(View.INVISIBLE);

                            //Toast.makeText(context , "ID : " + donarslist.get(position).getId() +" Position " +position , Toast.LENGTH_SHORT).show();
                        }
                    }).setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DeleteDonarAsyncTask delete = new DeleteDonarAsyncTask(context, donarslist.get(position).getId());
                            delete.execute();
                        }
                    }).setCancelable(true);
                    AlertDialog alertDialog = alert.create();
                    alertDialog.setTitle("Blood Donation Admin Panel");
                    alertDialog.show();
                }
                else if (context instanceof UserPanel) {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setIcon(R.drawable.mard);
                    View view1 = inflater.inflate(R.layout.dialog_layout , null);
                    alert.setView(view1);
                    int age;
                    age = 2017 - Integer.valueOf(donarslist.get(position).getDob());
                    String available;
                    int x;
                    x = 3 - Integer.valueOf(donarslist.get(position).getLast_donated());
                    if (x <= 0)
                    {
                        available = "Yes";
                    } else
                        available = "No";

                    TextView name = (TextView) view1.findViewById(R.id.dialog_name);
                    name.setText(donarslist.get(position).getName());

                    TextView donor_age = (TextView) view1.findViewById(R.id.dialog_age);
                    donor_age.setText(String.valueOf(age));

                    TextView bloodgroup = (TextView) view1.findViewById(R.id.dialog_bloodgroup);
                    bloodgroup.setText(donarslist.get(position).getBloodgroup());

                    TextView city = (TextView) view1.findViewById(R.id.dialog_city);
                    city.setText(donarslist.get(position).getCity());

                    TextView mobile = (TextView) view1.findViewById(R.id.dialog_mobile);
                    mobile.setText(donarslist.get(position).getMobile());

                    TextView available_donor = (TextView) view1.findViewById(R.id.dialog_available);
                    available_donor.setText(available);


                    alert.setCancelable(false).setPositiveButton("Call", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent call = new Intent(Intent.ACTION_DIAL , Uri.fromParts("tel",donarslist.get(position).getMobile() ,
                                    null));
                            startActivity(call);
                        }
                    }).setNegativeButton("Send Message", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                            smsIntent.setType("vnd.android-dir/mms-sms");
                            smsIntent.putExtra("address",donarslist.get(position).getMobile());
                            smsIntent.putExtra("sms_body","Need "+donarslist.get(position).getBloodgroup());
                            startActivity(smsIntent);
                        }
                    }).setCancelable(true);
                    AlertDialog alertDialog = alert.create();
                    alertDialog.setTitle("\t\t\t\tDonor Details");
                    alertDialog.show();


                }
            }
        });

        return view;
    }
}
