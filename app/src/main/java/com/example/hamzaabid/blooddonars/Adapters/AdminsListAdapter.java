package com.example.hamzaabid.blooddonars.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hamzaabid.blooddonars.POJO.Admin;
import com.example.hamzaabid.blooddonars.R;

import java.util.List;

/**
 * Created by Hamza Abid on 5/20/2017.
 */
public class AdminsListAdapter extends ArrayAdapter<Admin> {
    Activity activity;
    Context context;
    int imgres = R.drawable.mard;
    public AdminsListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Admin> objects) {
        super(context, resource, objects);
        activity = (Activity) context;
        this.context = context;
    }
    private static class ViewHolder {
        TextView adminname;
        TextView email;
        ImageView img;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder ;
        Admin admin = getItem(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView = inflater.inflate(R.layout.adminsilstviewlayout, parent, false);
            viewHolder.adminname = (TextView) convertView.findViewById(R.id.admin_name);
            viewHolder.email = (TextView) convertView.findViewById(R.id.admin_email);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.imageview);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AdminsListAdapter.ViewHolder) convertView.getTag();
        }
        viewHolder.adminname.setText(admin.getName());
        viewHolder.email.setText(admin.getEmail());
        viewHolder.img.setImageResource(imgres);

        return convertView;
    }
}
