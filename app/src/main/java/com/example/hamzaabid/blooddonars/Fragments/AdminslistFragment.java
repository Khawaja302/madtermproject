package com.example.hamzaabid.blooddonars.Fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.hamzaabid.blooddonars.Adapters.AdminsListAdapter;
import com.example.hamzaabid.blooddonars.POJO.Admin;
import com.example.hamzaabid.blooddonars.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminslistFragment extends Fragment {

    Activity activity;
    List<Admin> admins;

    public AdminslistFragment(Activity activity, List<Admin> admins) {
        this.activity = activity;
        this.admins = admins;
    }

    public AdminslistFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adminslist, container, false);

        ListView listView = (ListView) view.findViewById(R.id.adminslist);
        AdminsListAdapter adminsListAdapter = new AdminsListAdapter(activity , 0 , admins);
        listView.setAdapter(adminsListAdapter);

        return view;
    }

}
