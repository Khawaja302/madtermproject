package com.example.hamzaabid.blooddonars.Fragments;


import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.hamzaabid.blooddonars.NetworkCalls.AddDonarAsyncTask;
import com.example.hamzaabid.blooddonars.R;
import com.google.common.collect.Range;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddDonarFragment extends Fragment {

    Activity context;
    EditText name , dob , bloodgroup , city , mobile , lastdonated;

    public AddDonarFragment(Activity context) {
        this.context = context;
    }

    public AddDonarFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_donar, container, false);
        name = (EditText) view.findViewById(R.id.name);
        dob = (EditText) view.findViewById(R.id.dob);
        bloodgroup = (EditText) view.findViewById(R.id.bloodgroup);
        city = (EditText) view.findViewById(R.id.city);
        mobile = (EditText) view.findViewById(R.id.mobile);
        lastdonated = (EditText) view.findViewById(R.id.last_donated);

        final AwesomeValidation awesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
        awesomeValidation.setColor(Color.RED);

        // Validations
        awesomeValidation.addValidation(name , "[a-zA-Z\\s]+" , "Human Name Please");
        awesomeValidation.addValidation(dob , Range.closed(1970,2000) , "Enter a Valid year between 1970-2000");
        awesomeValidation.addValidation(bloodgroup , "\\A(A|B|AB|O)[+-]\\z", "Invalid Bloodgroup");
        awesomeValidation.addValidation(city , "[a-zA-Z\\s]+" , "Enter a city");
        awesomeValidation.addValidation(mobile , RegexTemplate.TELEPHONE , "Please Enter a Valid Mobile Number");
        awesomeValidation.addValidation(lastdonated , Range.closed(0,24) , "Invalid Range");


        Toast.makeText(context , "All fields are required ! " , Toast.LENGTH_SHORT).show();

        Button adddonar = (Button) view.findViewById(R.id.adddonar);
        adddonar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(awesomeValidation.validate()) {
                    AddDonarAsyncTask addDonarAsyncTask = new AddDonarAsyncTask(context, name.getText().toString()
                            , dob.getText().toString()
                            , bloodgroup.getText().toString()
                            , city.getText().toString()
                            , mobile.getText().toString()
                            , lastdonated.getText().toString());
                    addDonarAsyncTask.execute();
                }
            }
        });


        return view;
    }

}
