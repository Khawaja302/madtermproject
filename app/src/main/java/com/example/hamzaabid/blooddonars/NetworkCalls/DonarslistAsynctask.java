package com.example.hamzaabid.blooddonars.NetworkCalls;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Toast;

import com.example.hamzaabid.blooddonars.Activities.AdminPanel;
import com.example.hamzaabid.blooddonars.Activities.UserPanel;
import com.example.hamzaabid.blooddonars.POJO.Donar;
import com.example.hamzaabid.blooddonars.Activities.MainActivity;
import com.example.hamzaabid.blooddonars.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.example.hamzaabid.blooddonars.Fragments.ListViewFragment;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.hamzaabid.blooddonars.Activities.AdminPanel.fab;
import static com.example.hamzaabid.blooddonars.Activities.UserPanel.IP_ADDRESS;

/**
 * Created by Hamza Abid on 5/9/2017.
 */

public class DonarslistAsynctask extends AsyncTask<Object, Object, String> {
    Activity context;

    public DonarslistAsynctask(Activity context){
        this.context = context;
    }
    int responsecode;
    ProgressDialog loading;
    @Override
    protected void onPreExecute() {
        loading = ProgressDialog.show(context, "Fetching Data...", "Please wait...", false, false);
    }

    @Override
    protected String doInBackground(Object... params) {
        OkHttpClient client = new OkHttpClient();
        Response response;
        Request request = new Request.Builder().url("http://"+IP_ADDRESS+"/UolBloodDonationApp/api/uol").build();
        String json = null;
        try {
            response = client.newCall(request).execute();
            json = (response.body().string());
            responsecode = response.code();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    protected void onPostExecute(String jsondata) {
        if (responsecode == 200) {
            Gson gson = new Gson();
            List<Donar> donars = gson.fromJson(jsondata, new TypeToken<ArrayList<Donar>>() {
            }.getType());

            loading.dismiss();

            if (context instanceof UserPanel) {
                FragmentManager fragmentManager = ((UserPanel) context).getSupportFragmentManager();
                ListViewFragment listViewFragment = new ListViewFragment(context, donars);
                fragmentManager.beginTransaction().replace(R.id.main_container, listViewFragment).commit();
            } else if (context instanceof AdminPanel) {
                FragmentManager fragmentManager = ((AdminPanel) context).getSupportFragmentManager();
                ListViewFragment listViewFragment = new ListViewFragment(context, donars);
                fragmentManager.beginTransaction().replace(R.id.assetView_relative_layout, listViewFragment).commit();
            }
        }
        else {
            loading.dismiss();
            Toast.makeText(context, "Network Error", Toast.LENGTH_LONG).show();
        }
    }


}