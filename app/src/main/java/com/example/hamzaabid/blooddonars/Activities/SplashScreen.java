package com.example.hamzaabid.blooddonars.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.hamzaabid.blooddonars.Activities.MainActivity;
import com.example.hamzaabid.blooddonars.R;

public class SplashScreen extends AppCompatActivity {
    private static int SPLASH_TIMEOUT = 4000; // 1 second
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ImageView logo = (ImageView) findViewById(R.id.logo);

        logo.setImageResource(R.drawable.bloodlogo);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext() , UserPanel.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIMEOUT );

    }
}
