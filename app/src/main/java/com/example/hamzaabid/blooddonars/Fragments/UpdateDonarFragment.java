package com.example.hamzaabid.blooddonars.Fragments;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.example.hamzaabid.blooddonars.R;
import com.example.hamzaabid.blooddonars.NetworkCalls.UpdateDonarAsyncTask;
import com.google.common.collect.Range;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateDonarFragment extends Fragment {


    Activity context;
    EditText editname , editdob , editbloodgroup , editcity , editmobile , editlastdonated;
    String name , dob , bloodgroup , city , mobile , lastdonated;
    int id;
    public UpdateDonarFragment(Activity context, String name, String dob, String bloodgroup, String city, String mobile, String lastdonated , int id) {
        this.context = context;
        this.name = name;
        this.dob = dob;
        this.bloodgroup = bloodgroup;
        this.city = city;
        this.mobile = mobile;
        this.lastdonated = lastdonated;
        this.id = id;
    }

    public UpdateDonarFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_donar, container, false);
        editname = (EditText) view.findViewById(R.id.name);
        editdob = (EditText) view.findViewById(R.id.dob);
        editbloodgroup = (EditText) view.findViewById(R.id.bloodgroup);
        editcity = (EditText) view.findViewById(R.id.city);
        editmobile = (EditText) view.findViewById(R.id.mobile);
        editlastdonated = (EditText) view.findViewById(R.id.last_donated);

        editname.setText(name);
        editdob.setText(dob);
        editbloodgroup.setText(bloodgroup);
        editcity.setText(city);
        editmobile.setText(mobile);
        editlastdonated.setText(lastdonated);

        final AwesomeValidation awesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
        awesomeValidation.setColor(Color.RED);

        // Validations
        awesomeValidation.addValidation(editname , "[a-zA-Z\\s]+" , "Human Name Please");
        awesomeValidation.addValidation(editdob , Range.closed(1970,2000) , "Enter a Valid year between 1970-2000");
        awesomeValidation.addValidation(editbloodgroup , "\\A(A|B|AB|O)[+-]\\z", "Invalid Bloodgroup");
        awesomeValidation.addValidation(editcity , "[a-zA-Z\\s]+" , "Enter a city");
        awesomeValidation.addValidation(editmobile , RegexTemplate.TELEPHONE , "Please Enter a Valid Mobile Number");
        awesomeValidation.addValidation(editlastdonated , Range.closed(0,24) , "Invalid Range");


        Button update = (Button) view.findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (awesomeValidation.validate()) {
                    UpdateDonarAsyncTask update = new UpdateDonarAsyncTask(editname.getText().toString(),editdob.getText().toString(),editbloodgroup.getText().toString(), editcity.getText().toString(), editmobile.getText().toString(),editlastdonated.getText().toString(), id, context);
                    update.execute();
                }
            }
        });
        return view;
    }

}
